# Public Web

AltBil Website
==============

## Scope

* Run altbil website using Ghost
* Reference reproducable website deployment

## Future desires

* IPFS
* GPG Encrypted backups delivered to home servers

## Minimum scope (v0.1) - staging only (finished)

* Deploy
    * Ghost
        * volume: 2G

* Make a repo
    * Add deployment manifests
    * Add docker-compose if possible

* DNS
    * staging

## Ideal scope (v0.2)

* Content Migration

* Backup script
    * Create an export from ghost's API
    * tar them all
    * encrypt the tar for a few maintainer gpg keys
        * available in repo
        * also stored in a configmap
    * save the tar into specific target
        * any s3 target can be defined

* Privacy settings: https://github.com/TryGhost/Ghost/blob/master/PRIVACY.md/

## Ideal scope (v0.3) - first live

* Ghost Custom Storage
    * requires custom docker image
* Deploy
    * Minio
        * volume: 10G

* DNS
    * live

## Ideal scope (v0.4) - first live

* Backup script
    * Fetch all content bucket from minio
    * Create an export from ghost's API
    * tar them all
    * encrypt the tar for a few maintainer gpg keys
        * available in repo
        * also stored in a configmap
    * save the tar into minio
    * send email to maintainers (maybe)


## Full scope


## Repo
```
|- content/
|- template/
|- src/ (if there is any source)
|- k8s/manifests/
|- k8s/manifests/website-ingress.yaml
|- k8s/manifests/ghost-deployment.yaml (service and deployment)
|- k8s/manifests/ghost-pv.yaml
|- k8s/manifests/namespace.yaml
|- k8s/manifests/backup-cronjob.yaml
|- docker-compose.yaml
```